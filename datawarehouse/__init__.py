"""Datawarehouse Client."""
from cki_lib.logger import get_logger
from cki_lib.session import get_session
import requests_gssapi
from restclient import base

from datawarehouse import objects

LOGGER = get_logger("cki.datawarehouse_lib")
SESSION = get_session('cki.datawarehouse_lib')


class KCIDB:
    # pylint: disable=too-few-public-methods
    """KCIDB managers."""

    def __init__(self, api):
        """Initialize."""
        self.data = objects.KCIDBEndpointManager(api)
        self.checkouts = objects.KCIDBCheckoutManager(api)
        self.builds = objects.KCIDBBuildManager(api)
        self.tests = objects.KCIDBTestManager(api)
        self.testresults = objects.KCIDBTestResultManager(api)
        self.submit = objects.KCIDBSubmitManager(api)


class Datawarehouse:
    # pylint: disable=too-few-public-methods
    """Datawarehouse client."""

    def __init__(self, host, token=None, session=None):
        """Initialize."""
        session = session or SESSION
        if str(token).lower() == "oidc":
            LOGGER.info("Trying to authenticate via OIDC")
            session.get(
                f"{host}/oidc/authenticate/",
                auth=requests_gssapi.HTTPSPNEGOAuth(mutual_authentication=requests_gssapi.OPTIONAL),
                allow_redirects=True,
            ).raise_for_status()

            # Fill in headers required for CSRF validation
            session.headers.update({"Referer": host, "X-CSRFToken": session.cookies["csrftoken"]})

            # Clear token, authentication will come from session cookies
            token = None

        self.api = base.APIManager(host, token=token, session=session)

        self.pipeline = objects.PipelineManager(self.api)
        self.issue = objects.IssueManager(self.api)
        self.issue_regex = objects.IssueRegexManager(self.api)
        self.test = objects.TestManager(self.api)
        self.kcidb = KCIDB(self.api)
