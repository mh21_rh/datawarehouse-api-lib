# Datawarehouse API lib

This is a Python wrapper for the Datawarehouse API.

Information about the endpoints can be found in the Datawarehouse [Documentation](https://cki-project.gitlab.io/datawarehouse).

## How to install

```shell
python3 -m pip install git+https://gitlab.com/cki-project/datawarehouse-api-lib.git
```

## How to use

This module provides all the API endpoints in an easy way.

For read-only access of public data, no token is necessary.

```python
from datawarehouse import Datawarehouse
dw = Datawarehouse('https://datawarehouse.cki-project.org', token=None, session=None)
```

If you are a Red Hat associate, you can access internal data authenticating
through OpenID Connect using a Kerberos ticket, which you can get with
`kinit user@REALM`.

```python
from datawarehouse import Datawarehouse
dw = Datawarehouse('https://datawarehouse.internal.cki-project.org', token="oidc", session=None)
```

Note that, unless `session` is explicitly set, new instances of `Datawarehouse` will use the
same authorized session.

---

Besides the following examples, please check the source code for all the methods available.
All the endpoints listed on the [documentation] should be available.

### Get objects

To get a single element, for example a KCIDB Checkout, you can use the `get` attribute.

```python
In [2]: dw.kcidb.checkouts.get('28ad5f65c314ffdd5888d6afa61772d3032a332c')
Out[2]: KCIDBCheckout(id='28ad5f65c314ffdd5888d6afa61772d3032a332c', ...)
```

### List objects

To query a list endpoint, you can use the `list` attribute.

```python
In [5]: dw.kcidb.checkouts.list()
Out[5]: [KCIDBCheckout(id='80fdaa2813d9290be0b88e5eed9b8436046173b8', ...),
         KCIDBCheckout(id='7c04299291eb81f60fae466c06bb45836dda4f2f', ...),
         ...
]
```

### Nested objects

Some objects returned also have methods.

For example, when querying a Checkout, you can access it's Builds directly.

```python
In [6]: checkout = dw.kcidb.checkouts.get('28ad5f65c314ffdd5888d6afa61772d3032a332c')

In [7]: checkout
Out[7]: KCIDBCheckout(id='28ad5f65c314ffdd5888d6afa61772d3032a332c', ...)

In [8]: checkout.builds.list()
Out[8]: [KCIDBBuild(checkout_id='28ad5f65c314ffdd5888d6afa61772d3032a332c', id='redhat:930137', origin='redhat', ...),
         ...]
```

### Pagination

Pagination parameters are possible using kwargs.

```python
In [10]: dw.kcidb.checkouts.list(limit=1)
Out[10]: [KCIDBCheckout(id='bc43d5993e02622be8b5de3c9ae48814bb6e1e1c+059bdeb9c582b35490dc8b1a8178e26c3000de65a0878fffc82107b870e80cd8', ...)]

In [11]: len(dw.kcidb.checkouts.list(limit=2))
Out[11]: 2
```

[documentation]: https://cki-project.org/l/datawarehouse
